var vez = 0; //variavel que cervira como um contador para deifinir a vez de cada jogador
var terminado = false; //estado se alguem ganho o jogo ou não

// Realiza uma jogada
function selecionar(botao) {
    if (terminado == false){ //chacagem para caso de o jogo ja ter sido terminado
        if (botao.innerHTML == "") { //chacagem para caso a casa ja tenha sido preenchida
            var jogador = document.getElementById("indicadorDaVez"); // variavel que aponta, no HTML, para a div que mostrara o jogador da vez
            if (vez % 2 == 0){
                botao.innerHTML = "X"; // quando o contador "vez" for par ou 0, sera a vez de X, atribuindo sua marca dentro do botão escolhido pelo jogador
                jogador.innerHTML = "O"; // atribuição da vez do opente na rodada seguinte
            }
            else {
                botao.innerHTML = "O"; // quando o contador "vez" for impar, sera a vez de O, atribuindo sua marca dentro do botão escolhido pelo jogador
                jogador.innerHTML = "X"; // atribuição da vez do opente na rodada seguinte
            }
            vez++; //acrescimo ao contador vez para que ocorra a mudança de jogador
            ganhou(); //checagem para ver se alguem ganhou
        }
    }
}

// Zera todos as posições e recomeça o jogo
function resetar(){
    var jogador = document.getElementById("indicadorDaVez"); // variavel que aponta, no HTML, para a div que mostrara o jogador da vez
    var vencedor = document.getElementById("indicadorVencedor"); //variavel do indicador de vencedor
    jogador.style.display = "inline";
    document.getElementById("indicadorBase").style.display = "inline";
    vez = 0; // reseta o cantador
    jogador.innerHTML = "X"; // seta o indicador de vez para o primeiro jogador, no caso o X
    vencedor.innerHTML = null; // excluir a anunciação de um ganhador
    terminado = false; //recomçar o jogo
    for (var z = 0; z<9; z++) {
        document.getElementsByClassName("casa")[z].innerHTML = null; // reset de todas os 9 botão presentes no jogo
    }
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
    var vencedor = document.getElementById("indicadorVencedor"); //variavel do indicador de vencedor
    var casa = document.getElementsByClassName("casa"); //variavel com local das casas
    if (casa[0].innerHTML == "X" && casa[4].innerHTML == "X" && casa[8].innerHTML == "X") {
        vencedor.innerHTML = "O ganhador foi: X"; //anuncia que um jogador ganhou
        terminado = true; //terminar o jogo
    }
    if (casa[2].innerHTML == "X" && casa[4].innerHTML == "X" && casa[6].innerHTML == "X") {
        vencedor.innerHTML = "O ganhador foi: X"; //anuncia que um jogador ganhou
        terminado = true; //terminar o jogo
    }
    if (casa[0].innerHTML == "O" && casa[4].innerHTML == "O" && casa[8].innerHTML == "O") {
        vencedor.innerHTML = "O ganhador foi: O"; //anuncia que um jogador ganhou
        terminado = true; //terminar o jogo
    }
    if (casa[2].innerHTML == "O" && casa[4].innerHTML == "O" && casa[6].innerHTML == "O") {
        vencedor.innerHTML = "O ganhador foi: O"; //anuncia que um jogador ganhou
        terminado = true; //terminar o jogo
    }
    for (var z = 0; z<=6; z = z + 3) {
        if (casa[z].innerHTML == "X" && casa[z+1].innerHTML == "X" && casa[z+2].innerHTML == "X") {
            vencedor.innerHTML = "O ganhador foi: X"; //anuncia que um jogador ganhou
            terminado = true; //terminar o jogo
            break;
        }
        else if (casa[z].innerHTML == "O" && casa[z+1].innerHTML == "O" && casa[z+2].innerHTML == "O") {
            vencedor.innerHTML = "O ganhador foi: O"; //anuncia que um jogador ganhou
            terminado = true; //terminar o jogo
            break;
        }
    }
    for (var z = 0; z<3; z++) {
        if (casa[z].innerHTML == "X" && casa[z+3].innerHTML == "X" && casa[z+6].innerHTML == "X") {
            vencedor.innerHTML = "O ganhador foi: X"; //anuncia que um jogador ganhou
            terminado = true; //terminar o jogo
            break;
        }
        else if (casa[z].innerHTML == "O" && casa[z+3].innerHTML == "O" && casa[z+6].innerHTML == "O") {
            vencedor.innerHTML = "O ganhador foi: O"; //anuncia que um jogador ganhou
            terminado = true; //terminar o jogo
            break;
        }
    }
    if (vez > 8 && vencedor.innerHTML == "") {
        vencedor.innerHTML = "Deu velha"; // checagem para dar velha
        terminado = true; //terminar o jogo
    }
    if (terminado == true){
        document.getElementById("indicadorDaVez").style.display = "none"; //tirando a visibilidade do indicador de vez quando ha um ganhador
        document.getElementById("indicadorBase").style.display = "none"; //tirando a visibilidade do indicador de vez quando ha um ganhador
    }
}
var quest = -1; //contador para mostrar a questão atual (começa em -1, pois inicialmente não sera mostrado questao)
var pontuacao = 0; //contader de quantos pontos o usuario obteve por resposta

//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'Ohana v.2',
        respostas: [
            { valor: 2, texto: 'Ela é legal, mas vamo so se vê' },
            { valor: 0, texto: 'Tenho é nojo, vou me esconder' },
            { valor: 1, texto: 'Não me importo, iteragir pra que?' },
            { valor: 3, texto: 'É nunca abandonar ou esquecer' }
        ]
    }
]

function mostrarQuestao() {
    var titulo = document.getElementById("titulo"); //setando a variavel com o local do "titulo"
    var resposta = document.getElementsByTagName("span"); //setando a variavel com o local dos textos das opções alternativas de escolha
    var opcoes = document.getElementsByClassName("opcoes"); //setando a variavel com o local das opções de pergunta
    if (quest == -1 || preenchido() == true || quest == perguntas.length) { //condição para prosseguir com o quiz, sempre precisando uma opção estar selecioanda
        document.getElementById("listaRespostas").style.display = "block"; //alterando a visibilidade das opções de texto para visivel
        for (var z = 0; z < opcoes.length; z++) { 
            if (opcoes[z].checked == true) { //condição para identificar a resposta escolhida
                pontuacao = pontuacao + parseInt(opcoes[z].value); //aumentando a pontuação com base na resposta escolhida
            }
            opcoes[z].checked = false; //dismarcando a opção para o começo da proxima rodada
        }
        if (quest >= perguntas.length - 1){
            finalizarQuiz(); //finalizando o jogo com base no numero de perguntas
            quest++; //adicionando o contador para andamento do codigo
        }
        else {
            document.getElementById("confirmar").innerHTML = "Próxima"; //mudando o texto do botão
            quest++; //proceguindo para a proxima questão
            titulo.innerHTML = perguntas[quest].texto; //colocando o proximo titulo
            for (z = 0; z < resposta.length; z++) {
                resposta[z].innerHTML = perguntas[quest].respostas[z].texto; //atualizando as respostas
                opcoes[z].value = perguntas[quest].respostas[z].valor; //atualizando o valor de cada pontuação de resposta
            }
        }
    }
}

function finalizarQuiz() {
    document.getElementById("listaRespostas").style.display = "none"; //tirando a visibilidade das respostas
    document.getElementById("resultado").style.display = "block"; //disponibilizando a visibilidade do resultado
    document.getElementById("titulo").innerHTML = "QUIZ DOS VALORES DA GTI"; //alterando o titulo
    pontuacao = (pontuacao / (3 * perguntas.length)) * 100; //adaptando a pontuação para porcentagem de acerto
    document.getElementById("resultado").innerHTML = "Você obteve " + pontuacao.toFixed(2) + "% de acerto"; //printando o resultado
    document.getElementById("confirmar").innerHTML = "Recomeçar"; //alterando o texto do botão
    if (quest == perguntas.length) { //condição para recomeçar o jogo
        quest = -2; //resetando o contador de questão
        pontuacao = 0; //resetando a pontuação
        document.getElementById("resultado").style.display = "none"; //tirando a visibilidade do resultado
        document.getElementById("confirmar").innerHTML = "COMEÇAR"; //alterando o texto do botão
    }
}

function preenchido() { //função para checar se ha alguma opção marcada
    var opcoes = document.getElementsByClassName("opcoes"); //variavel de armazenamento dos locais de resposta
    var marcação = false; //variavel que define se ha algum item marcado
    for (var z = 0; z < opcoes.length; z++) {
        if (opcoes[z].checked == true) {
            marcação = true; //indicando que existe um item marcado
        }
    }
    return marcação; //retorndo true casa tenha um item marcado ou false caso contrario
}